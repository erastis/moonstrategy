import os
import pymongo
import pandas as pd
import numpy as np
from datetime import datetime, date, timedelta
from colorama import Back, Fore, Style

# Connect to MongoDB
client = pymongo.MongoClient(os.environ.get('DBCO'))
db = client["Lab"]
dbcolumn = db["Moon"]

# Parameters (Max:2014-02-01)
startDate = '2021-01-01'
endDate = '2021-08-01'
coin = 'BTC'
currency = 'USD'

rawdata = pd.DataFrame(list(dbcolumn.find()))

df = rawdata.loc[:, 'Datetime':'Position'][(rawdata.loc[:, 'Datetime'] >= startDate) &
                                           (rawdata.loc[:, 'Datetime'] <= endDate)]

df.reset_index(drop=True, inplace=True)


def backtestStrat():

    # Moon Strategy
    signalDetails = []
    pos = 0
    num = 0
    percentChange = []

    for i in df.index:
        price = df['BTC'][i]
        date = df['Datetime'][i]
        if df['Position'][i] == 'BUY':
            if (pos == 0):
                buyPrice = price
                pos = 1
                signalDetails.append(
                    "Buying at " + str(int(buyPrice)) + "" + str(date))

        elif df['Position'][i] == 'SELL':
            if (pos == 1):
                sellPrice = price
                pos = 0
                signalDetails.append("Selling at " +
                                     str(int(sellPrice)) + "" + str(date))
                sumPercent = (sellPrice/buyPrice-1)*100
                percentChange.append(sumPercent)

        if(num == df['Position'].count()-1 and pos == 1):
            sellPrice = price
            pos = 0
            signalDetails.append("Selling last opened position at " +
                                 str(int(sellPrice)) + "" + str(date))
            percent = (sellPrice/buyPrice-1)*100
            percentChange.append(percent)

        num += 1

    # Tested Strategy ROI
    roi = round(np.sum(percentChange), 2)

    return(roi)


def hodlStrat():

    # HODL Strategy
    firstPrice = df['BTC'].iloc[0]
    lastPrice = df['BTC'].iloc[-1]

    roiHodl = round(((lastPrice - firstPrice) / firstPrice) * 100, 2)

    return(roiHodl)


def dcaStrat():

    # DCA Strategy
    coinDCA = []
    period = 0

    for i in df.index:
        if (i % 30 == 0):
            coinDCA.append(float(1 / df['BTC'].iloc[i]))
            period += 1

    roiDCA = round((((np.sum(coinDCA) * df['BTC'].iloc[-1]) -
                     (int(period))) / (int(period))) * 100, 2)

    return(roiDCA)


if __name__ == '__main__':
    strat = backtestStrat()
    hodl = hodlStrat()
    dca = dcaStrat()
    print(f'Coin : {Back.YELLOW}{Fore.BLACK}{coin}{Style.RESET_ALL} \nCurrency : {Back.YELLOW}{Fore.BLACK}{currency}{Style.RESET_ALL} \nFrom {Back.YELLOW}{Fore.BLACK}{startDate}{Style.RESET_ALL} to {Back.YELLOW}{Fore.BLACK}{endDate}{Style.RESET_ALL} \n\nMoon Strategy ROI: {Back.YELLOW}{Fore.BLACK}{strat}%{Style.RESET_ALL} \nHodl ROI: {Back.YELLOW}{Fore.BLACK}{hodl}%{Style.RESET_ALL} \nDCA ROI: {Back.YELLOW}{Fore.BLACK}{dca}%{Style.RESET_ALL}')

###### Moon Strategy

After reading several articles about the correlation between BTC price and moon phases, I decided to analyse the "Moon Strategy" which suggests to buy during the full moon phase and sell during the new moon phase. Obviously the strategy is based on false beliefs, but I wanted to confront statistics with these crazy theories. Therefore, I  backtested the strategy and compare it to the HODL and DCA Strategies. I have to admit the Year-to-Date performance is unexpectedly good... but spoiler alert: Do not take it seriously, do your own research as it will always pay off in the long term 😉

###### Results

![](data/results.png)

